# K

A bash script that provides aliases for kubectl commands, resources, and options.

This will save you a lot of typing!

Download, alias, or add to your path.

Check the [bash script](k) for the full list.

# Basics

```
$ k g po -W -n ks
NAME                          READY   STATUS    RESTARTS      AGE    IP          NODE   NOMINATED NODE   READINESS GATES
coredns-76f75df574-gdkxc      1/1     Running   2 (13m ago)   114m   10.88.0.6   cp0    <none>           <none>
[...]
```

```
$ k g no -N
node/cp0
[...]
```

## Watch it!

I prefer using `watch -n1` over `--watch`, so `w` is a special alias.

```
$ k w g no -n ks
Every 1.0s: kubectl get pods --namespace kube-system

NAME                          READY   STATUS    RESTARTS   AGE
coredns-76f75df574-dmhwv      1/1     Running   0          2m11s
coredns-76f75df574-jn92b      1/1     Running   0          2m11s
etcd-cp0                      1/1     Running   0          2m11s
[...]
```

## Make it your own!

Please copy this in whatever way you want to and edit in place for yourself.

If you think you changes would be useful to everyone, please create an issue or a merge request.
